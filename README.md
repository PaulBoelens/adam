#Instructions

### Installaton

```$ npm install```

### Content

Place content inside the designated folder under:

```./public/resources```

### Run application

```$ npm start```

Open a browser and goto: ```http://localhost:3000/```
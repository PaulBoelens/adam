const MEDIA_VIDEO = 0
const MEDIA_AUDIO = 1

let MIN_DURATION = 5
let MAX_DURATION = 10
let MEDIA_VOLUME = 0.5

const SHOW_ELEMENT = 'show-element'
const HIDE_ELEMENT = 'hide-element'

/** Menu */

const menu = document.getElementById('menu')

// Hide/show the menu
const toggleMenu = (() => {
    menu.className = (menu.className === SHOW_ELEMENT)?HIDE_ELEMENT:SHOW_ELEMENT
})

// Initialise the menu key binding
const initMenuKeyInput = (() => {
    document.addEventListener('keydown', function(event) {
        // 13 = enter/return
        switch(event.keyCode) {
        case 13:
            // Prevent any default actions attached to the keyinput
            event.preventDefault()
            toggleMenu()
            break
        default:
            break
        }
    })
})

// Media duration
let maxDurationOutput = null
const setMaxDurationOutput = (value) => {
    if (!maxDurationOutput) maxDurationOutput = document.getElementById('media_max_duration_output')

    // Render the max duration
    maxDurationOutput.innerHTML = 'max duration: '+ value
    
    // Set the max duration
    MAX_DURATION = value
}

let minDurationOutput = null
const setMinDurationOutput = (value) => {
    if (!minDurationOutput) minDurationOutput = document.getElementById('media_min_duration_output')

    // Render the min duration
    minDurationOutput.innerHTML = 'min duration: '+ value

    // Set the mi duration
    MIN_DURATION = value
}

let maxDuration = null
const initMaxDuration = () => {
    if (!maxDuration) maxDuration = document.getElementById('media_max_duration_input')
    
    maxDuration.oninput = (e) => {
        reCalibrateSliders()
        setMaxDurationOutput(e.currentTarget.value)
    }
}

let minDuration = null
const initMinDuration = () => {
    if (!minDuration) minDuration = document.getElementById('media_min_duration_input')

    minDuration.oninput = (e) => {
        reCalibrateSliders()
        setMinDurationOutput(e.currentTarget.value)
    }
}

const reCalibrateSliders = (() => {
    const reset = (slider, min, max) => {
        slider.min = min
        slider.max = max
    }
    
    reset(maxDuration, minDuration.value, maxDuration.max)
    reset(minDuration, minDuration.min, maxDuration.value-1)
})


//Volume
let mediaVolumeOutput = null
const setMediaVolumeOutput = (value) => {
    if (!mediaVolumeOutput) mediaVolumeOutput = document.getElementById('media_volume_output')

    // Render the volume
    mediaVolumeOutput.innerHTML = 'volume: '+ value
    
    // Set the max duration
    MEDIA_VOLUME = value
}

let mediaVolume = null
const initMediaVolume = () => {
    if (!mediaVolume) mediaVolume = document.getElementById('media_volume')
    
    mediaVolume.oninput = (e) => {
        adjustMediaVolume(e.currentTarget.value)
        setMediaVolumeOutput(e.currentTarget.value)
    }
}

const adjustMediaVolume = ((vol) => {
    audioPlayer.volume = vol
    videoPlayer.volume = vol
})

/** Calculations and logic */

const getRandomStart = ((min, duration) => {
    return Math.floor(Math.random()*(duration-MIN_DURATION+1)+MIN_DURATION)
})

const getRandomRange = ((min, max) => {
    const rand0 = Math.floor(Math.random()*(max-min+1)+min)
    let rand1 = Math.floor(Math.random()*(max-min+1)+min)

    if (rand0 === rand1) rand1++

    // Add to the minduration duration when this one is not meet
    if (rand0-rand1 < MIN_DURATION) rand1+=MIN_DURATION
    
    return (rand0 < rand1)?[rand0, rand1]:[rand1, rand0]
})


/** Interval */

let mediaEndingTimeout = null
const setMediaEndingTimeout = ((end, done) => {
    // Clear a previous timeout if it is running
    if (mediaEndingTimeout) clearTimeout(mediaEndingTimeout)

    mediaEndingTimeout = setTimeout( () => {
        // The custom function that can be handled
        done()
        // Start the roulette to get render new media
        mediaRoulette()
        
    }, end*1000)
})

const clearEndingTimeout = (() => {
    if (mediaEndingTimeout) clearTimeout(mediaEndingTimeout)
})

/** Video player */

const initVideoPlayer = (() => {    
    videoPlayer.onloadedmetadata = handleMetaData
    videoPlayer.onended = handleEnding
})

const showVideo = (() => {
    videoPlayer.className = SHOW_ELEMENT
})

const hideVideo = (() => {
    videoPlayer.className = HIDE_ELEMENT
})

/** Audio player */

const audioWrapper = document.getElementById('audio-wrapper')
const audioPlayer = document.getElementById('audio-player')
const audioImage = document.getElementById('audio-image')

const initAudioPlayer = (() => {    
    audioPlayer.onloadedmetadata = handleMetaData
    audioPlayer.onended = handleEnding
})

const showAudio = (() => {
    audioWrapper.className = SHOW_ELEMENT
})

const hideAudio = (() => {
    audioWrapper.className = HIDE_ELEMENT
})

/** Media Roulette */

let previousFileLocation = null

const loadFile = ((ftype, f) => {
    const getFile = () => dirlist[ftype][Math.floor(Math.random()*(size-0+1))]
    const size = dirlist[ftype].length-1
    let file = getFile()
    while(file === previousFileLocation) file = getFile()
    f.src = 'resources/'+ftype+'/'+file

    previousFileLocation = file
})

const mediaRoulette = (() => {
    const key = Math.floor(Math.random()*(1-0+1))

    switch(key) {
    case MEDIA_VIDEO:
        
        loadFile('video', videoPlayer)
        
        showVideo()
        hideAudio()
        break
    case MEDIA_AUDIO:

        loadFile('audio', audioPlayer)
        loadFile('image', audioImage)
        
        hideVideo()
        showAudio()
        break
    }
})

const handleEnding = (() => {

    // Clear the timeout that could be running
    clearEndingTimeout()

    // Spin the roulette again...
    mediaRoulette()
})

const handleMetaData = ((e) => {
    // Get the total duration of the media
    const duration = e.currentTarget.duration
    // Set the range when the duration is longer than the calculated duration
    if (duration > MAX_DURATION-MIN_DURATION) {
        // Get a random range for the start and end time
        const range = getRandomRange(MIN_DURATION, MAX_DURATION)

        let randomDuration = (range[1]-range[0])
        let start = getRandomStart(MIN_DURATION, duration)

        if (randomDuration+start > duration) {
//            randomDuration = duration
            start = 0
        }

        // Make sure to set the min duration 
        if (randomDuration > duration || randomDuration < MIN_DURATION) randomDuration = MIN_DURATION

        console.log('start:', start, ', duration:', randomDuration)

        // Set the start time
        e.currentTarget.currentTime = start
        
        // Start the timeout with the ending
        setMediaEndingTimeout(randomDuration, () => {
            if (!videoPlayer.paused) videoPlayer.pause()
            if (!audioPlayer.paused) audioPlayer.pause()
        })
    }
    e.currentTarget.duration = 1

    // Start the media
    e.currentTarget.play()
})

/** Fetching data */

let dirlist = {}

const fetchDirLists = ((done) => {
    fetch('/lists', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then( (res) => {
        if (res.ok) {
            res.json().then( (data) => {
                dirlist = data
                done()
            })
        } else {
            done()
        }
    }).catch( (err) => {
        done()
        console.error('fetch:', err)
    })
})


/** Init */

const videoPlayer = document.getElementById('video-player')

document.addEventListener('DOMContentLoaded', function onReady() {
    
    // First fetch all the lists (async)
    fetchDirLists(() => {
        
        // Initialise the input keys to show/hide the menu
        initMenuKeyInput()

        // Init the duration handlers
        initMinDuration()
        initMaxDuration()
        reCalibrateSliders()

        initMediaVolume()
        adjustMediaVolume(MEDIA_VOLUME)

        initVideoPlayer()
        initAudioPlayer()

        // Initial start of the media roulette
        mediaRoulette()
    })
    
}, false)

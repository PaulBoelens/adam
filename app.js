/**
 * Main App.js
 *
 * @author info@paulboelens.nl
 */

const express = require('express')
const app = express()

const fs = require('fs')
const path = require('path')
const mime = require('mime')

const videoList = fs.readdirSync(path.resolve(__dirname, './public/resources/video')).filter( (file) => {
    return mime.lookup(file).indexOf('video/') === 0
})

const audioList = fs.readdirSync(path.resolve(__dirname, './public/resources/audio')).filter( (file) => {
    return mime.lookup(file).indexOf('audio/') === 0
})

const imageList = fs.readdirSync(path.resolve(__dirname, './public/resources/image')).filter( (file) => {
    return mime.lookup(file).indexOf('image/') === 0
})



app.use(express.static(path.join(__dirname, 'public')))

app.get('/lists', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.send(JSON.stringify({
        video: videoList,
        audio: audioList,
        image: imageList
    }))
})

app.listen(3000, () => {
    console.log('Server listening on port 3000')
})


